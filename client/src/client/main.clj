(ns client.main
  (:require [clj-http.client :as client]
            [clojure.data.json :as json]))


(defn sent-recieve[host port id]
  (let [result (client/get (str "http://" host ":" port "/forecasts/" id)
               {:accept :json, :throw-exceptions false})]
    (if (= 200 (:status result))
      (println (json/read-str (:body result)))
      (println "Error: " (:status result) (:body result)))))

(defn -main [host port id]
  (sent-recieve host port id))
