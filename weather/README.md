# weather

Super weather restful service

## Prerequisites

You will need [Leiningen][1] 1.7.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    mongod --dbpath ./data/db

    lein ring server

    lein run host port


