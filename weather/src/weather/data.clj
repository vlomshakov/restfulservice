(ns weather.data
  (:use [monger.core :only [connect! set-db! get-db]]
        [monger.result :only [ok?]]
        [monger.result :only [has-error?]])
  (:require [monger.collection :as mc]
            [monger.util :as util]
            [monger.joda-time]
            [monger.json]))

(def mongo-options
  {:host "localhost"
   :port 27017
   :db "forecasts"
   :forecast-collection "forecast-by-city"
   :log-collection "request-log"})

(connect! mongo-options)
(set-db! (get-db (mongo-options :db)))



(defn get-all-forecasts [] (mc/find-maps (mongo-options :forecast-collection)))
(defn update-forecast [forecast id] (mc/update-by-id (mongo-options :forecast-collection) id forecast))
(defn delete-forecast [id] (ok? (mc/remove-by-id (mongo-options :forecast-collection) id)))
(defn delete-all-forecasts [] (ok? (mc/remove (mongo-options :forecast-collection))))

(defn get-forecast [id] (mc/find-maps (mongo-options :forecast-collection) {:_id id}))


(defn- next-task-id []
  (inc (apply max -1 (map #(% :_id)  (get-all-forecasts)))))

(defn- with-id
  [forecast]
  (assoc forecast :_id (next-task-id)))

(defn create-forecast
  [forecast]
  (let [item-forecast (with-id forecast)]
    (if (ok? (mc/insert (mongo-options :forecast-collection) item-forecast))
      item-forecast
      nil)))

;
; logging
;
(defn add-hist
  [info]
  (ok? (mc/insert (mongo-options :log-collection) info)))

(defn get-log [] (mc/find-maps (mongo-options :log-collection)))

(defn get-frequent-method
  []
  (map
      (fn[it]
        {:freq (count (mc/find-maps (mongo-options :log-collection) {:req-method it}))
         :method it})
      ["GET" "POST" "PUT" "DELETE" "OPTIONS"]))