(ns weather.logging
  (:use compojure.core
        ring.util.response
        [clojure.string :only [upper-case]])
  (:require [weather.data :as data]))




(defn wrap-request-persist-logger
  [handler]
  (fn [req]
    (let [{remote-addr :remote-addr request-method :request-method uri :uri} req]
      (data/add-hist {:remote-addr remote-addr :req-method (upper-case (name request-method)) :uri uri})
      (handler req))))

