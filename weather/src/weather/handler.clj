(ns weather.handler
  (:use compojure.core
        ring.adapter.jetty ring.handler.dump
        ring.util.response
        weather.logging
        [ring.middleware.file :only [wrap-file]]
        [ring.middleware.file-info :only [wrap-file-info]]
        [ring.middleware.format-response :only [wrap-restful-response]])
  (:require [compojure.handler :as handler]
            [compojure.route :as route]
            [weather.model :as model]
            [weather.util :as util]))


(defroutes api-routes

  (context "/forecasts" []
    (GET "/" [:as req]
      (model/get-all-forecasts req))
    (GET ["/:id", :id #"\d+"] [:as req]
      (model/get-forecast req))
    (POST "/" [:as req]
      (model/create-forecast req))
    (PUT ["/:id", :id #"\d+"] [:as req]
      (model/update-forecast req))
    (DELETE "/" [:as req]
      (model/delete-all-forecasts req))
    (DELETE ["/:id", :id #"\d+"] [:as req]
      (model/delete-forecast req))
    (ANY "/" []
      (response "Not supported")))


  (OPTIONS "/" []
    (util/options [:options :get :put :post :delete]
      "{'options':[\n
          { 'method':'GET', 'path':'/forecasts', 'params':[] },\n
          { 'method':'GET', 'path':'/forecasts/:id', 'params':[] },\n
          { 'method':'GET', 'path':'/log', 'params':[] },\n
          { 'method':'GET', 'path':'/state', 'params':[] },\n
          { 'method':'OPTIONS', 'path':'/', 'params':[] },\n
          { 'method':'POST', 'path':'/forecasts', 'form-params':['city'] },\n
          { 'method':'DELETE', 'path':'/forecasts/:id', 'params':[] },\n
          { 'method':'DELETE', 'path':'/forecasts/', 'params':[] },\n
          { 'method':'PUT', 'path':'/forecasts/:id', 'form-params':['city'] },]}"))
  (GET "/log" []
    (model/get-log))
  (GET "/stat" []
    (model/get-stat))
; debug open api weather
;  (GET "/" []
;    (model/api-weather {:city "s"}))

  (route/not-found "Page not found"))

(def app
  (->
    (handler/api api-routes)
    (wrap-request-persist-logger)
    (wrap-file "resources/public/img")
    (wrap-file-info)
;    (wrap-request-logging)
    (wrap-restful-response)))

(defn -main [host port]
  (run-jetty app {:port (Integer. port) :host host}))

; client sent request fro data and get object and auto serialized it
