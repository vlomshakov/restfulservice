(ns weather.model
  (:use weather.views
        weather.util
        ring.util.response
        [validateur.validation]
        [clojure.walk :only [keywordize-keys]])
  (:require [ weather.data :as data]
            [clj-http.client :as client]
            [cheshire.core :as cheshire]))

(def forecast-task-validator (validation-set
                                (presence-of :city)))

(defn predict-weather
  "mock weather predictor"
  [target]
  (merge target {:temp 20 :description "sunny"}))


(defn api-weather
  [target]
  (let [city (target :city)
        resp (cheshire/decode ((keywordize-keys (client/get (str "http://api.openweathermap.org/data/2.1/find/name?q=" city "&type=like"))) :body))]
    (let [cod (resp "cod")]
      (if (= cod "200")
        (do
          (apply merge  target [
                                 {:temp (int (- (double (get-in resp ["list" 0 "main" "temp"])) 273.15))}
                                 {:description (get-in resp ["list" 0 "weather" 0 "icon"])}]))
        (do
          (merge target {:temp "???" :error true}))))))

(defn wrap-respons-format
  [types resp]
    (defn support? [pattern types] (if (re-find pattern types) true false))
    (cond
      (support? #"text/html" types) (-> resp html-view response (header "Content-Type" "text/html"))
      (support? #"text/plain" types) (-> resp text-view response (header "Content-Type" "text/plain"))
      (support? #"application/json" types) (-> resp json-view response (header "Content-Type" "application/json"))
      (support? #"application/xml" types) (-> resp xml-view response (header "Content-Type" "application/xml"))
      (support? #"\*/\*" types) (-> resp text-view response (header "Content-Type" "text/plain"))
      :else (create-msg "Unsupported accept" 406)))

(defn get-all-forecasts
  [req]
  (let [types ((req :headers) "accept")]
    (wrap-respons-format types   (map #(api-weather %) (data/get-all-forecasts)))))

(defn get-forecast
  [req]
  (let [id (-> req :params :id)
        forecast (data/get-forecast (Integer. id))
        types ((req :headers) "accept")]
      (if (empty? forecast)
        (create-msg "Forecast not found" 404)
        (wrap-respons-format types (map #(api-weather %) forecast)))))

(defn create-forecast
  [req]
  (let [new-task (-> req :form-params keywordize-keys)]
    (if (valid? forecast-task-validator new-task)
      (if (contains? (api-weather new-task) :error)
        (create-msg "City not found" 404)
        (let [result (data/create-forecast new-task)] ;OMG spaghetti!
          (if (not= nil result)
            (let [location (url-from req (str (result :_id)))]
              (->
                (response "Success")
                (status 201)
                (header "Location" location)))
            (create-msg "Task was already created" 400))))
      (create-msg "Bad request: city is not specified in body" 400))))

(defn delete-forecast
  [req]
  (let [id (-> req :params :id Integer.)
        isContained (-> id data/get-forecast empty? not)]
    (if isContained
      (do
        (data/delete-forecast id)
        (create-msg "Success" 200))
      (create-msg "Bad request: task isn't existed" 400))))

(defn delete-all-forecasts
  [req]
  (let [isContainedAny (-> (data/get-all-forecasts) empty? not)]
    (if isContainedAny
      (do
        (data/delete-all-forecasts)
        (create-msg "Success" 200))
      (create-msg "Bad request: task collection is empty" 400))))

(defn update-forecast
  [req]
  (let [new-task (-> req :form-params keywordize-keys)
        id (-> req :params :id Integer.)
        isContained (-> id data/get-forecast empty? not)]
    (if (and (valid? forecast-task-validator new-task))
      (if (contains? (api-weather new-task) :error)
        (create-msg "City not found" 404)
        (if isContained
          (do
            (data/update-forecast new-task id)
            (create-msg "Success" 200))
          (create-msg "Bad request: task isn't exist" 500)))
      (create-msg "Bad request: city is not specified in body" 400))))


;
; logging and stat
;

(defn get-log
  []
  (->
    (data/get-log)
    log-view))

(defn get-stat
  []
  (->
    (data/get-frequent-method)
    stat-view))



