(ns weather.views
  (:use [hiccup core page]
        clojure.data.xml))

(defn html-view
  [forecasts]
;  (defn get-icon
;    [desc]
;    (cond
;      (= desc "sunny") [:image {:src "/sunny.jpeg" :height "42" :width "42"}]
;      (= desc "Clouds") [:image {:src "/cloudy.png" :height "42" :width "42"}]
;      (= desc "snow") [:image {:src "/snow.jpg" :height "42" :width "42"}]
;      :else [:image]))

  (html5
    [:title "Weather forecast"]
    [:body
      [:h1 "This is uberuseful weather web widget!"
       [:br] "Add own widget by city"]
      (map (fn[x]
              [:div
                [:h2 (str "City: " (:city x))]
                [:h2 (str "Current temp: " (:temp x))]
                ;(get-icon (:description x))
                [:image {:src (str "http://openweathermap.org/img/w/" (:description x) ".png")}]]) forecasts)]))


(defn text-view
  [forecasts]
  (apply str "This is uberuseful weather web widget!" \newline
             (map #(str "City: " (:city %) \newline
             "Current temp: " (:temp %) \newline
             "Description: " (:description %) \newline) forecasts)))

(defn xml-view
  [forecasts]
  (emit-str
    (element :forecasts { }
      (map (fn[it]
        (let [{n :_id c :city t :temp d :description} it]
        (element :forecast {:id n}
        (element :city {} c)
        (element :temp {} (str t))
        (element :desc {} d)))) forecasts))))


; conversion is delegated wrap-restful-response
(defn json-view
  [forecast]
  forecast)


(defn log-view
  [logs]
  (html5
    [:title "Log"]
    [:body
     [:h1 "Request history"]
     [:table {:border "1"}
     (mapcat (fn[x]
             [[:tr
               [:td "Address"]
               [:td (:remote-addr x)]]
             [:tr
               [:td "method"]
               [:td (:req-method x)]]
             [:tr
               [:td "uri"]
               [:td (:uri x)]]]) logs)]]))

(defn stat-view
  [stats]
  (html5
    [:title "Stat"]
    [:body
     [:h1 "Frequency of using every request method"]
     [:table {:border "1"}
      [:tr [:td "Method"] [:td "Freq"]]
      (map (fn[x]
                 [:tr
                  [:td (:method x)]
                  [:td (:freq x)]]) stats)]]))