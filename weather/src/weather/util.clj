(ns weather.util
  (:use ring.util.response
        [clojure.string :only [upper-case join]]))

(defn options
  ([] (options #{:options} nil))
  ([allowed] (options allowed nil))
  ([allowed body]
    (->
      (response body)
      (header "Allow" (join ", " (map (comp upper-case name) allowed))))))

(defn create-msg
  ([body code]
    (->
      (response body)
      (status code)))
  ([] (create-msg "Not implemented." 501)))

(defn url-from
  [{scheme :scheme server-name :server-name server-port :server-port uri :uri}
   & path-elements]
  (str "http://" server-name ":" server-port  uri "/" (join "/" path-elements)))