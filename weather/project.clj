(defproject weather "0.1.0-SNAPSHOT"
  :description "Simple rest service for getting weather"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [compojure "1.1.5"]
                 [ring-middleware-format "0.3.1"]
                 [ring/ring-json "0.2.0"]
                 [clj-time "0.6.0"]
                 [com.novemberain/monger "1.7.0-beta1"]
                 [com.novemberain/validateur "1.5.0"]
                 [org.clojure/data.xml "0.0.7"]
                 [clj-http "0.7.7"]
                 [cheshire "5.2.0"]]
  :plugins [[lein-ring "0.8.8"]
            [lein-kibit "0.0.8"]]
  :ring {:handler weather.handler/app}
  :main ^:skip-aot weather.handler
  :profiles
    {:dev
     {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}})
